.. package_name documentation master file, created by
   sphinx-quickstart on Tue Sep  4 23:37:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to package_name's documentation!
========================================

Hi there 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   package_modules.rst
   examples.rst
   test_status.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
