import os, shutil, time

class badge():
    '''Attach badge'''
    def __init__(self, test_name = 'test_01'):
        self.test_name = test_name
        self.badge_path = '../../badges/'
        shutil.copy(self.badge_path+'failed.png', \
                    os.getcwd()+'/'+test_name+'_badge.png')

        now_string = time.strftime("%d %b %Y, %H:%M:%S", time.localtime())
        f = open(os.getcwd()+'/'+test_name+'_badgeTime.txt', "w")
        f.write(now_string)
        f.close()

    def completed(self):
        shutil.copy(self.badge_path+'succeeded.png', \
                    os.getcwd()+'/'+self.test_name+'_badge.png')

def propagate_badge(test_name = 'test_01'):
    print(test_name)


        